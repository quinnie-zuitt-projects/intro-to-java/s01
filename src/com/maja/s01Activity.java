package com.maja;

public class s01Activity {
    public static void main(String[] args){
        String name = "Quinnie";
        int age = 27;
        String faveBand = "Red Velvet";
        boolean isSingle = true;

        System.out.println("Hi! I am "+ name + "," + age + " years old.");
        System.out.println("My favorite band is " + faveBand + " and yes, it is " + isSingle + ", I am single.");
    }
}
